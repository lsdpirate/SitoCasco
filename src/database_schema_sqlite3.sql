create table Users(
	id integer primary key autoincrement,
	username varchar(30) not null,
	pwd varchar(30) not null,
	mail varchar(250) not null
);

create table Devices(
	id integer primary key autoincrement,
	serial_ varchar(64) not null,
	owner_id integer not null,
	api_key varchar(24) not null,
	friendly_name varchar(36) not null,
	foreign key (owner_id) references Users(id)
);

create table Log(
	id integer primary key autoincrement,
	data mediumtext not null,
	sync_date datetime not null,
	device_id integer not null,
	foreign key(device_id) references Devices(id)
);

create table Settings(
	id integer primary key autoincrement,
	log_before_accident integer not null, -- In seconds
	log_after_accident integer not null, -- In seconds
	sync_frequency smallint not null, -- In hours
	wifi_ssid varchar(64) not null,
	wifi_pwd varchar(64) not null,
	wifi_sec_type varchar(5) not null,
	device_id integer not null,
	foreign key(device_id) references Devices(id)
);

create table Sets(
	id integer primary key autoincrement,
	user_id integer not null,
	settings_id integer not null,
	device_id integer not null,
	foreign key (user_id) references Users(id),
	foreign key (device_id) references Devices(id),
	foreign key (settings_id) references Settings(id),
);