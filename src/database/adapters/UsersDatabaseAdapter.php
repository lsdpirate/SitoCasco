<?php
include_once __DIR__."/../DatabaseSetup.php";
include_once __DIR__."/../connectors/DatabaseConnector.php";
include_once __DIR__."/../../model/User.php";

class UsersDatabaseAdapter{

	public static function getUserByName($name){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$result = $conn->executeQuery("SELECT * FROM Users WHERE username = ?", [$name]);
		if(!isset($result[0])){
			return null;
		}
		$result = $result[0];
		$user = new User($result["id"], $result["username"], $result["pwd"], $result["mail"]);
		return $user;
	}

	public static function getUserByID($id){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$result = $conn->executeQuery("SELECT * FROM Users WHERE id = ?", [$id]);
		if(!isset($result[0])){
			return null;
		}
		$result = $result[0];
		$user = new User($result["id"], $result["username"], $result["pwd"], $result["mail"]);
		return $user;
	}

	public static function updateUser($user){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$result = $conn->executeDMLQuery("UPDATE Users SET username = ?, pwd = ?, mail = ? WHERE id = ?", 
			[$user->getUsername(), $user->getPassword(), $user->getMail(), $user->getID()]);
		if($result !== 1){
			throw new Exception("Could not update the user $user", 1);
		}
		return $result === 1;
	}

	public static function removeUserFromDatabase($user){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$result = $conn->executeDMLQuery("DELETE FROM Users WHERE id = ?", [$user->getID()]);
		if($result !== 1){
			throw new Exception("Could not remvove $user, not found in database.", 1);
		}
		return $result === 1;
	}

	public static function addUserToDatabase($user){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$result = $conn->executeDMLQuery("INSERT INTO Users (username, pwd, mail) VALUES(?, ?, ?)",
			[$user->getUsername(), $user->getPassword(), $user->getMail()]);
		if($result !== 1){
			throw new Exception("Could not add the user", 1);
		}
		return $result === 1;
	}

}