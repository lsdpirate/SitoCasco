<?php
include_once __DIR__."/../DatabaseSetup.php";
include_once __DIR__."/../connectors/DatabaseConnector.php";
include_once __DIR__."/../../model/Setting.php";

class SettingsDatabaseAdapter{

	public static function getSettingByDeviceID($deviceID){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$setting = $conn->executeQuery(
			"SELECT * FROM Settings ss join Sets s on (s.settings_id = ss.id) WHERE s.device_id = ?", [$deviceID]);
		if(!isset($setting[0])){
			return null;
		}
		$setting = $setting[0];
		return new Setting($setting["settings_id"], $setting["log_before_accident"], $setting["log_after_accident"], 
			$setting["sync_frequency"], $setting["wifi_ssid"], $setting["wifi_pwd"], $setting["wifi_sec_type"], $deviceID);
	}

	public static function updateSetting($setting){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$result = $conn->executeDMLQuery("UPDATE Settings SET log_before_accident = ?, log_after_accident = ?, sync_frequency = ?, wifi_ssid = ?, wifi_pwd = ?, wifi_sec_type = ? WHERE id = ?", 
			[$setting->getLogTimeBeforeAccident(), $setting->getLogTimeAfterAccident(), 
			$setting->getSynchronizationFrequency(), $setting->getWifiSSID(),
			$setting->getWifiPassword(), $setting->getWifiSecurityType(), $setting->getID()]);
		if($result !== 1){
			throw new Exception("Could not update setting $setting", 1);
		}
	}

	public static function removeSetting($setting){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$result = $conn->executeDMLQuery("DELETE FROM Settings WHERE id = ?", [$setting->getID()]);
		if($result !== 1){
			throw new Exception("Could not remove setting $setting", 1);
		}	
	}

	public static function addSetting($setting, $userID){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$result = $conn->executeDMLQuery(
			"INSERT INTO Settings(log_before_accident, log_after_accident, sync_frequency, wifi_ssid, wifi_pwd, wifi_sec_type, device_id)
			VALUES(?, ?, ?, ?, ?, ?, ?)", 
			[$setting->getLogTimeBeforeAccident(), $setting->getLogTimeAfterAccident(), 
			$setting->getSynchronizationFrequency(), $setting->getWifiSSID(),
			$setting->getWifiPassword(), $setting->getWifiSecurityType(),
			$setting->getDeviceID()]);
		if($result !== 1){
			throw new Exception("Could not add setting $setting", 1);
		}
		$conn->executeDMLQuery("INSERT INTO Sets(user_id, settings_id, device_id) VALUES(?, ?, ?)",
			[$userID, $conn->getLastInsertedID(), $setting->getDeviceID()]);

	}
}
