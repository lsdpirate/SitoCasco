<?php
include_once __DIR__."/../DatabaseSetup.php";
include_once __DIR__."/../connectors/DatabaseConnector.php";
include_once __DIR__."/../../model/Log.php";

class LogsDatabaseAdapter{

	/**
	 * TODO: Make this function return a list of Log.
	 * @param  [type] $deviceID [description]
	 * @return [type]           [description]
	 */
	public static function getLogByDeviceID($deviceID){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$queryResult = $conn->executeQuery("SELECT * FROM queryResult WHERE device_id = ?", [$deviceID]);
		if(!isset($queryResult[0])){
			return null;
		}
		$result = array();
		foreach($queryResult as $log){
			array_push($result, new Log($log["id"], $log["data"], date_create($log["sync_date"]), $log["device_id"]));
		}
		return $result;
	}

	public static function getLogById($id){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$queryResult = $conn->executeQuery("SELECT * FROM Log WHERE id = ?", [$id]);
		if(!isset($queryResult[0])){
			return null;
		}
		$log = $queryResult[0];
		$log = new Log($log["id"], $log["data"], date_create($log["sync_date"]), $log["device_id"]);
		return $log;
	}

	public static function addLogToDatabase($log){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$result = $conn->executeDMLQuery("INSERT INTO Log(data, sync_date, device_id) VALUES(?, ?, ?)",
			[$log->getData(), $log->getSyncDate()->format(DateTime::W3C), $log->getDeviceID()]);
		if($result !== 1){
			throw new Exception("Could not add the log", 1);
		}
	}

	public static function removeLogFromDatabase($logID){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$result = $conn->executeDMLQuery("DELETE FROM Log WHERE id = ?", [$logID]);
		if($result !== 1){
			throw new Exception("Could not add the log", 1);
		}	
		return $result === 1;
	}

	public static function updateLog($log){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$result = $conn->executeDMLQuery("UPDATE Log SET data = ?, sync_date = ?, device_id = ? WHERE id = ?", 
			[$log->getData(), $log->getSyncDate()->format(DateTime::W3C), $log->getDeviceID(), $log->getID()]);
		if($result !== 1){
			throw new Exception("Could not update the log $log", 1);
		}

	}

	public static function getLastLogByDevice($device){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$result = $conn->executeQuery("SELECT id, data as data, max(sync_date) as sync_date, device_id FROM log GROUP BY device_id HAVING device_id = ?", 
			[$device->getID()]);
		if(!isset($result[0])){
			return null;
		}
		$result = $result[0];
		$log = new Log($result["id"], $result["data"], date_create($result["sync_date"]), $result["device_id"]);
		return $log;
	}

	public static function getLogsMetadataByUser($user){
		$conn = DatabaseConnector::$DATABASE_CONNECTOR;
		$queryResult = $conn->executeQuery("SELECT DATE(l.sync_date) as date, count(*) as cnt, l.id as id, length(data) * 8 as size, d.friendly_name as friendly_name
			FROM log l join devices d on (d.id = l.device_id) 
			GROUP BY date(l.sync_date) 
			HAVING d.owner_id = ?",
			array($user->getID()));
		$result = array();
		foreach ($queryResult as $row) {
			$toAdd = array($row['date'], $row['cnt'], $row['id'], $row["size"], $row["friendly_name"]);
			array_push($result, $toAdd);
		}
		return $result;
	}
}