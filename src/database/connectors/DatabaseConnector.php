<?php

abstract class DatabaseConnector{
	public static $DATABASE_CONNECTOR = null;
	public abstract function executeQuery($sql, $parameters = []);
	public abstract function executeDMLQuery($sql, $parameters = []);
	public abstract function close();
	public abstract function getLastInsertedID();
}