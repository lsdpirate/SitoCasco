<?php

include_once __DIR__."/DatabaseConnector.php";

class SQLiteConnector extends DatabaseConnector{

	public function __construct($databaseLocation = __DIR__."/../../database.db"){
		$this->pdo = new PDO("sqlite:$databaseLocation");
		$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		DatabaseConnector::$DATABASE_CONNECTOR = $this;
	}

	public function executeQuery($sql, $parameters = []){
		$stmt = $this->pdo->prepare($sql);
		$stmt->execute($parameters);
		return $stmt->fetchAll();
	}

	public function executeDMLQuery($sql, $parameters = []){
		$stmt = $this->pdo->prepare($sql);
		$stmt->execute($parameters);
		return $stmt->rowCount();
	}

	public function close(){
		$this->pdo = null;
		DatabaseConnector::$DATABASE_CONNECTOR = null;
	}

	public function getLastInsertedID(){
		return $this->pdo->lastInsertId();
	}
}