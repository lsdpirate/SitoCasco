<?php
include_once __DIR__."/control/HomeController.php";

$controller = new HomeController();
try{
	$controller->run();
}catch (Exception $e){
	include_once __DIR__."/control/ErrorController.php";
	$errorController = new ErrorController($e);
	$errorController->run();
}