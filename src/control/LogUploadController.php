<?php

class LogUploadController{

	public function renderUploadPage(){
		$context = array();
		include_once __DIR__."/../view/uploadView.php";
	}

	public function handleLogUpload(){
			/*
				- Input sanitization
				- Better error messages
				- Find better way to put the file in the database
			 */
			include_once __DIR__."/../database/adapters/LogsDatabaseAdapter.php";
			include_once __DIR__."/../database/adapters/UsersDatabaseAdapter.php";
			include_once __DIR__."/../database/adapters/DevicesDatabaseAdapter.php";
			include_once __DIR__."/../model/Log.php";

			$apiKey = filter_input(INPUT_POST, "api_key");
			if($apiKey === null){
				throw new Exception("Missing API key parameter", 1);
			}

			if(!isset($_FILES["log_file"])){
				throw new Exception("Missing log file in request", 1);
			}

			$file = $_FILES["log_file"];
			$fileContent = file_get_contents($file["tmp_name"]);
			$device = DevicesDatabaseAdapter::getDeviceByAPIKey($apiKey);
			$user = UsersDatabaseAdapter::getUserByID($device->getOwnerID());

			if($device === null){
				throw new Exception("Could not find the device", 1);
			}

			if($user === null){
				throw new Exception("Could not find the user associated with the device", 1);
			}

			$log = new Log(null, $fileContent, new DateTime(), $device->getID());
			LogsDatabaseAdapter::addLogToDatabase($log);
	}

	function handleLogDeletion(){
		session_start();
		$log_id = filter_input(INPUT_GET, "log_id");

		if($log_id === null){
			throw new Exception("Missing log id parameter", 1);
		}

		include_once __DIR__."/../database/adapters/LogsDatabaseAdapter.php";
		include_once __DIR__."/../database/adapters/UsersDatabaseAdapter.php";
		include_once __DIR__."/../database/adapters/DevicesDatabaseAdapter.php";
		include_once __DIR__."/../model/Log.php";
		include_once __DIR__."/../model/Device.php";

		$log = LogsDatabaseAdapter::getLogById($log_id);

		if($log === null){
			return;
		}

		$device = DevicesDatabaseAdapter::getDeviceByID($log->getDeviceID());

		if($device === null){
			throw new Exception("There is no device associated with that log", 1);
		}

		$user = UsersDatabaseAdapter::getUserByName($_SESSION["username"]);
		if($user === null || $user->getID() != $device->getOwnerID()){
			throw new Exception("You are not authorized to delete this file", 1);
		}

		$res = LogsDatabaseAdapter::removeLogFromDatabase($log->getID());
		if($res){
			http_response_code(204);
		}else{
			http_response_code(404);
		}
	}

	public function run(){

		if($_SERVER["REQUEST_METHOD"] === "GET"){
			$this->renderUploadPage();

		}elseif($_SERVER["REQUEST_METHOD"] === "POST"){
			$this->handleLogUpload();
		
		}elseif ($_SERVER["REQUEST_METHOD"] === "DELETE") {
			$this->handleLogDeletion();
		}
	}
}