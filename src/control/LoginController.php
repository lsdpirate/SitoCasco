<?php

/**
 * Do login, render login page, handle login requests
 */
class LoginController{

	public function renderLoginPage(){
		$context = array();
		include_once __DIR__."/../view/loginView.php";
	}

	public function run() {
		session_start();
		if(isset($_SESSION["username"])){
			header("Location: home.php");
			return;
		}

		$requestMethod = $_SERVER["REQUEST_METHOD"];
		if($requestMethod == "GET"){
		
			LoginController::renderLoginPage();
		
		}elseif($requestMethod == "POST"){
			include_once __DIR__."/../database/adapters/UsersDatabaseAdapter.php";
			$user = UsersDatabaseAdapter::getUserByName($_POST["username"]);
			if($user === null){
				throw new Exception("The user doesn't exist", 1);
			}else{
				if($user->getPassword() === $_POST["password"]){
					$_SESSION["username"] = $user->getUsername();
					header("Location: home.php");
				}else{
					throw new Exception("Invalid credentials", 1);
				}
			}
		}
	}
}