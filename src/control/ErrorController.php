<?php

class ErrorController{
	
	public function __construct($exception){
		$this->exception = $exception;
	}

	public function renderErrorPage(){
		$context = array("message" => $this->exception->getMessage());
		include_once __DIR__."/../view/errorView.php";
		die();
	}
	public function run(){
		http_response_code(404);
		$this->renderErrorPage();

	}
}