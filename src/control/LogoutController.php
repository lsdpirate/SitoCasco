<?php

class LogoutController{

	public function run(){
		session_start();
		session_destroy();
		header("Location: login.php");
	}
}