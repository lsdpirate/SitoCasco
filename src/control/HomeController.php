<?php

class HomeController{

	public function renderHomePage(){
		$context = array("username" => $this->user->getUsername());
		include_once __DIR__."/../database/adapters/DevicesDatabaseAdapter.php";
		include_once __DIR__."/../database/adapters/LogsDatabaseAdapter.php";
		include_once __DIR__."/../model/Device.php";
		$userDevices = DevicesDatabaseAdapter::getDevicesByOwnerUsername($this->user->getUsername());
		$context["devices"] = $userDevices;
		$lastLogs = array();
		foreach ($userDevices as $device) {
			$lastLog = LogsDatabaseAdapter::getLastLogByDevice($device);
			array_push($lastLogs, $lastLog);
		}
		$context["lastLogs"] = $lastLogs;

		$page = filter_input(INPUT_GET, "page");
		if ($page === null){
			$page = "dashboard";
		}
		$context["page"] = $page;
		include_once __DIR__."/../view/homeView.php";
	}

	public function run(){
		session_start();
		$username = isset($_SESSION["username"]) ? $_SESSION["username"] : null;
		if($username === null){
			header("Location: login.php");
		}
		include_once __DIR__."/../database/adapters/UsersDatabaseAdapter.php";

		$this->user = UsersDatabaseAdapter::getUserByName($username);
		$this->renderHomePage();
		
	}
}