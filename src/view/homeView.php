<!DOCTYPE html>
<html>
	<head>
		<title>Casco - Dashboard</title>
		<link rel="stylesheet" type="text/css" href="/static/css/reset.css">
		<link rel="stylesheet" type="text/css" href="/static/css/typography.css">
		<link rel="stylesheet" type="text/css" href="/static/css/dashboard.css">
		<link rel="stylesheet" type="text/css" href="/static/css/dropdown.css">
		<script src="https://use.fontawesome.com/d57327b423.js"></script>
		<script
  		src="https://code.jquery.com/jquery-3.2.1.min.js"
  		integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  		crossorigin="anonymous"></script>
	</head>
	<body>
		<div class="app-wrapper">
			<div class="board-selector">
				<div class="user-info-container">
					<div class="dropdown">
						<button onclick="myFunction()" class="dropbtn">
							<?= $context['username']; ?>
							<font style="float: right; font-size: 12px; padding-top: 5px">▼</font>
						</button>
						<div id="myDropdown" class="dropdown-content">
							<a class="normal-text" href="/logout.php" style="font-size: 12px">Logout</a>
						</div>
					</div>
				</div>
				<div class="dash-selector-button<?php $context['page'] === 'dashboard'? print('-selected') : null?>" id="dashboard-selector">
					<div class="dash-selector-wrapper">
						<i class="fa fa-tachometer" aria-hidden="true" style="padding-left: 4px"></i>
						<font class="normal-text">Dashboard</font>
					</div>
					
				</div>
				<div class="dash-selector-button<?php $context['page'] === 'devices'? print('-selected') : null?>" id="devices-selector">
					<div class="dash-selector-wrapper">
						<i class="fa fa-wrench" aria-hidden="true"></i>
						<font class="normal-text">Dispositivi</font>
					</div>
				</div>
				<div class="dash-selector-button<?php $context['page'] === 'logs'? print('-selected') : null?>" id="logs-selector">
					<div class="dash-selector-wrapper">
						<i class="fa fa-area-chart" aria-hidden="true"></i>
						<font class="normal-text">Log</font>
					</div>
				</div>
			</div>
			<div class="dashboard-wrapper">
				<div class="dash-content">
					<?php
						$page = $context['page'];
							include_once __DIR__."/dashboard/".$page.".php";
					?>
				</div>
			</div>
		</div>
	</body>

	<script type="text/javascript">
		/* When the user clicks on the button, 
		toggle between hiding and showing the dropdown content */
		function myFunction() {
		    document.getElementById("myDropdown").classList.toggle("show");
		}

		// Close the dropdown menu if the user clicks outside of it
		window.onclick = function(event) {
		  if (!event.target.matches('.dropbtn')) {

		    var dropdowns = document.getElementsByClassName("dropdown-content");
		    var i;
		    for (i = 0; i < dropdowns.length; i++) {
		      var openDropdown = dropdowns[i];
		      if (openDropdown.classList.contains('show')) {
		        openDropdown.classList.remove('show');
		      }
		    }
		  }
		}

		$("#dashboard-selector").click(() => {window.location.href = "/home.php?page=dashboard";});
		$("#logs-selector").click(() => {window.location.href = "/home.php?page=logs";});
		$("#devices-selector").click(() => {window.location.href = "/home.php?page=devices";});

	</script>
</html>