<style type="text/css">
	.graph-wrapper{
		width: 65vw;
		height: 50vh;
		margin: auto;
	}

	.log-list-wrapper{
		width: 500px;
		height: 30%;
		overflow-y: auto;
		margin-top:	15px;
		flex-shrink: 1;
		flex-grow: 0;
		border-style: solid;
		border-width: 1px;
		border-color: #ddd;
	}

	.log-list{
		width: 100%;
		height: 100%;
	}

	.log-element{
		height: 50px;
		font-size: 16px;
		background-color: #fff;
		/*margin-top: 10px;*/
		border-bottom-style: solid;
		border-bottom-width: 1px;
		border-bottom-color: #ddd;
	}

	.log-element.selected{
		background-color: #b90504;
		color: #e8e8e8;
	}

	.log-element > div{
		height: 100%;
		padding-top: 15px;
		padding-left: 5px;
		font-size: 18px;
	}

	#graph-container{
		width: 100%;
		height: 100%;
	}

	.button-wrapper{
		margin-top: 15px;
		width: 300px;
	}

	.btn{
		border-radius: 7px;
		border-style: solid;
		border-width: 1px;
		padding: .8em;
		padding-left: 1em;
		padding-right: 1em;
		font-size: 16px;
		color: #ffffff;
	}

	.confirm{
		border-color: #5cb85c;
		background-color: #5cb85c;

	}

	.cancel{
    	border-color: #c50000;
   		background-color: #e42020;
	}

	.btn[disabled]{
		display: none;
	}

	.bottom-content{
		display: flex;
		justify-content: space-between;
		margin-top: 10px;
		width: 65vw;
		margin: auto;
		margin-top: 10px;
	}

	.details-container{
		width: 400px;
		flex-grow: 0;
		flex-shrink: 1;
	    padding: 10px;
	    border-style: solid;
	    border-width: 1px;
	    border-color: white;
	    background-color: white;
	    margin-top: 15px;
	}

	.detail-row{
		line-height: 3em;
	}

	.detail-label{
		font-weight: bold;
		margin-right: 1em;
	}

	.box-shadow{
		-webkit-box-shadow: 1px 1px 5px 0px rgba(166,166,166,1);
        -moz-box-shadow: 1px 1px 5px 0px rgba(166,166,166,1);
		box-shadow: 1px 1px 5px 0px rgba(166,166,166,1);
	}

</style>

<div style="height: 100%">
	<div class="dash-content-title">
		<font class="heading-text">
			Logs
		</font>
	</div>
	<br>
	<div class="graph-wrapper">
		<div class="box-shadow">
			<div id="graph-container">
			
			</div>
		</div>
	</div>
	<div class="bottom-content">
		<div class="log-list-wrapper box-shadow">
			<div class="log-list">
			</div>
		</div>
		<div class="details-container box-shadow">
			<p class="detail-row"><font class="normal-text detail-label">Data:</font><font class="normal-text" id="date-detail">19/6/17</font></p>
			<p class="detail-row"><font class="normal-text detail-label">Durata:</font><font class="normal-text" id="duration-detail">30s</font></p>
			<p class="detail-row"><font class="normal-text detail-label">Dispositivo:</font><font class="normal-text" id="device-detail">device detail</font></p>
			<p class="detail-row"><font class="normal-text detail-label">Dimensioni file:</font><font class="normal-text" id="size-detail">size detail</font></p>
			<div class="button-wrapper">
				<button class="btn confirm normal-text" id="download-button" disabled>Scarica</button>
				<button class="btn cancel normal-text" id="delete-button" disabled>Elimina</button>
			</div>		

		</div>
	</div>
	<iframe src="" id="download-frame" style="display: none;"></iframe>
</div>

<script src="https://code.highcharts.com/highcharts.src.js"></script>
<script type="text/javascript">
	var chart = Highcharts.chart('graph-container', {

    title: {
        text: 'Accelerazione'
    },
    yAxis: {
        title: {
            text: 'm/s^2'
        }
    },
    series: [
    	{
    		name : "Accelerazione",
    		data : [],
    	},
    ],
    width: "100%",

    responsive: {
 		rules: [{
    		condition: {
      			maxWidth: 500
    		},
    	chartOptions: {
      		legend: {
        		enabled: false
      		}
    	}
  	}]
}

});

	// Set the callback for a log in the loglist
	var setLogClickCallback = (e, c, s, d, dn) => {
		e.click(() => {
			$.ajax("/viewlog.php", {
				data : {
					log_id: c
				},

				success : (data) => {
					var toDisplay = [];
					console.log(data);
					// Parse the data to be plotted on the graph
					data.split("\n").forEach((line) => {
						if (line.charAt(0) == "#"){
							return;
						}
						toDisplay.push(parseFloat(line.split(";")[0]));
					});
					chart.series[0].setData(toDisplay, true, true);
					console.log(toDisplay);
					console.log("data was set");

					// Set the correct callbacks for the delete and the download buttons
					$(".btn").prop("disabled", false);
					$("#download-button").click(() => {
						console.log("Downloading file...");
						$("#download-frame").prop("src", "viewlog.php?log_id=" + c + "&download=1");
					});

					$("#delete-button").click(() => {
						console.log(c);
						$.ajax("/upload.php?log_id=" + c, {
							method: "DELETE",

							success : (data, response) => {
								if(response === "nocontent"){
									$(".btn").prop("disabled", true);
									e.remove();
								}
							}
						})
					});

					$("#date-detail").text(d);
					$("#duration-detail").text();
					$("#device-detail").text(dn);
					$("#size-detail").text(s + " B");
					$(".log-element").removeClass("selected");
					$(e).addClass("selected");
				}

			});
		});
	}

	$.getJSON("/viewlog.php", {
		metadata : 1
	},
		(data) => {
			console.log(data);
			for (var i = 0; i < data['ids'].length; i++) {
				var div = $("<div></div>");
				div.addClass("log-element");
				div.addClass("normal-text");
				var innerDiv = $("<div></div>");
				innerDiv.text(data['dates'][i]);
				setLogClickCallback(div, data["ids"][i], data["sizes"][i], data["dates"][i], data["device_names"][i]);
				div.append(innerDiv);
				$(".log-list").append(div);
			}
	});


</script>