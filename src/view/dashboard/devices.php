<style type="text/css">
	.devices-list{
		flex-grow: 0;
		flex-shrink: 1;
		width: 300px;
		height: 100%;
		overflow-y: auto;
		padding: 5px;
	}

	.content-wrapper{
		display: flex;
		height: calc(85vh - 80px);
	}

	.settings-container{
		flex-grow: 1;
		flex-shrink: 1;
		padding-left: 20px;
		height: 100%;
	}

	.device-container{
		height: 55px;
		margin-bottom: 5px;
		background-color: #fff;
	}

	.device-container.selected{
		background-color: #b90504;
		color: #fff;
	}

	.device-name{
		font-size: 16px;
		font-weight: 600 !important;
	}

	.device-serial{
		font-size: 14px;
	}

	input{
		border-radius: 8px;
		border-style: solid;
		border-width: 1px;
		border-color: rgba(0, 0, 0, 0.25);
		padding: 7px;
		font-size: 16px;

	}

	input:focus{
		box-shadow: 0px 0px 5px 0px #b90504;
		outline-width: 0px;
	}

	input[type=number]{
		width: 3em;
	}

	.btn{
		border-radius: 7px;
		border-style: solid;
		border-width: 1px;
		padding: .8em;
		padding-left: 1em;
		padding-right: 1em;
		font-size: 16px;
		color: #fff;
	}

	.btn[disabled]{
		display: none;
	}

	.confirm{
		border-color: #5cb85c;
		background-color: #5cb85c;

	}

	.cancel{
    	border-color: #c50000;
   		background-color: #e42020;
	}

	.input-label{
		font-size: 1.5vmax;
	}

	.input-group{
		margin-bottom: 15px;
	}

	.labels-container{
		width: auto;
		flex-grow: 1;
		flex-shrink: 0;
		display: flex;
		flex-direction: column;
		justify-content: space-around;
		height: 300px;
		padding: 5px;
	}

	.input-container{
		width: auto;
		flex-grow: 1;
		display: flex;
		flex-direction: column;
		flex-shrink: 1;
		justify-content: space-around;
		height: 300px;
		max-width: 300px;
		padding: 5px;
	}
	
	.box-shadow{
		-webkit-box-shadow: 1px 1px 5px 0px rgba(166,166,166,1);
        -moz-box-shadow: 1px 1px 5px 0px rgba(166,166,166,1);
		box-shadow: 1px 1px 5px 0px rgba(166,166,166,1);
	}

</style>
<div style="height: 100%;">
	<div class="dash-content-title">
		<font class="heading-text">
			Dispositivi
		</font>
	</div>
	<div class="content-wrapper">
		<div class="devices-list">
			<div style="padding-top: 20px;">
				<?php 
					foreach($context['devices'] as $device){
				?>

				<div class="device-container box-shadow" 
					onclick="renderDeviceSettings('<?= $device->getAPIKey(); ?>', '<?= addslashes($device->getFriendlyName()); ?>')">
					<div style="padding-top: 10px; padding-left: 5px;">
						<div class="device-name">
							<font class="bold-text">
								<?= $device->getFriendlyName();?>
							</font>
						</div>
						<div class="device-serial">
							<font class="normal-text">
								<?= $device->getSerial();?>
							</font>
						</div>
					</div>
				</div>		
				<?php
					}
				?>
			</div>
		</div>
		<div style="flex-grow: 1; max-width: 900px;">
			<div class="settings-container">
				<div style="height: 50px; font-size: 48px; margin-bottom: 20px;">
					<font class="heading-text" id="device-name" style="font-size: 2.5rem"></font>
				</div>
				<div style="display: flex; justify-content: space-between; flex-direction: row; width: 100%;">
					<div class="labels-container">
						<font class="input-label normal-text">Registrazione pre incidente (s):</font> 
						<font class="input-label normal-text">Registrazione post incidente (s):</font>
						<font class="input-label normal-text">Frequenza sincronizzazione (gg):</font>
						<font class="input-label normal-text">SSID Wifi:</font> 
						<font class="input-label normal-text">Password Wifi:</font> 
						<font class="input-label normal-text">Sicurezza Wifi:</font> 
					</div>
					<div class="input-container">
						<input class="normal-text" type=number name="log_time_before_accident" min="1" max="20" maxlength="2" disabled>
						<input class="normal-text" type=number name="log_time_after_accident" min="1" max="20" disabled>
						<input class="normal-text" type=number name="sync_freq" min="1" max="60" disabled>
						<input class="normal-text" type="text" name="wifi_ssid"  maxlength="64" disabled>
						<input class="normal-text" type="password" name="wifi_pwd" maxlength="64" disabled>
						<input class="normal-text" type="text" name="wifi_enc_type" maxlength="5" disabled>
					</div>
				</div>
				<div style="padding-left: 100px;">
					<button class="btn confirm" name="save-settings" disabled>Salva</button>
					<button class="btn cancel" name="cancel-settings" disabled>Annulla</button>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
	function commitSettings(apiKey){
		var jsonData = {};
		$("input").each((index, element) => {
			var el = $(element)
			jsonData[el.attr("name")] = el.val();
		});
		console.log(jsonData);
		$.ajax("settings.php", {
			method: "POST",
			data:{
				settings : JSON.stringify(jsonData),
				api_key : apiKey
			},

			success: (data, content) => {
			},
			
			error: (error, status, ex) => {
				console.log(error);
				console.log(status);
				console.log(ex);
			}

		});
	}
	function renderDeviceSettings(deviceAPIKey, deviceFriendlyName){
		$.ajax("settings.php", {
			method: "GET",
			data:{
				api_key: deviceAPIKey
			},
			success: (data) => {
				console.log(data);
				Object.keys(data).forEach((element) => {
					$(`input[name=${element}]`).val(data[element]);
					$(`input[name=${element}]`).prop("disabled", false);
				});
			},
			error: (error, status, ex) => {
				console.log(error);
				console.log(status);
				console.log(ex);
			}
			
		});
		$("#device-name").text(deviceFriendlyName);

		$(".btn").prop("disabled", false);
		
		$(".btn.confirm").off("click");
		$(".btn.confirm").click(() => {
			commitSettings(deviceAPIKey);
		});
	}

	function resetInputs(){
		$("input").prop("disabled", true);
		$("input").val(null);
		$(".btn").prop("disabled", true);
		$("#device-name").text("");
	}

	$(".btn.cancel").click(() => {
		resetInputs();
		$(".device-container").removeClass("selected");
	});

	$(".device-container").click((e) => {
		$(".device-container").removeClass("selected");
		$(e.currentTarget).addClass("selected");
		console.log(e.currentTarget);
		console.log("Clicked");
	});
</script>