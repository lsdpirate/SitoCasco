<style type="text/css">
	@import url('https://fonts.googleapis.com/css?family=Open+Sans:400,600');
	.device-recap-container{
		width: 250px;
		height: 200px;
	}
	.devices-list{
		display: flex;
		padding-top: 35px;
	}
	
	.device-recap-header{
		text-align: center;
		color: #B90504;
		font-size: 18px;
		text-align: left;
		border-bottom-style: solid;
		border-bottom-width: 1px;
		border-bottom-color: #B90504;
		padding: 5px;
		font-family: 'Open Sans', sans-serif;
		font-weight: 600;
	}
	.device-recap-container{
		margin-right: 30px;
		border-bottom: #B90504;
		border-left: #B90504;
		border-top: #B90504;
		border-right: #B90504;
		border-style: solid;
		border-width: 1px;
		background-color: #F6F6F6;
		border-radius: 3px;
		box-shadow: 2px 2px 5px 0px #c7c7c7;
		font-size: 16px;
	}

	.device-recap-content{
		padding: 5px;
		font-family: 'Open Sans', sans-serif;
		font-weight: 400;
	}

	#graph-container{
		width: 525px;
		height: 300px;
		border-style: solid;
		border-color: #B90504;
		border-width: 1px;
		padding: 1px;
		box-shadow: 2px 2px 5px 0px #c7c7c7;
	}

	.recent-activity{
		padding-top: 50px;
	}

</style>
<div>
	<div class="dash-content-title">
		<font class="heading-text">Dashboard</font>
	</div>
	<div class="devices-list">
		<?php
			for ($i=0; $i < count($context["devices"]); $i++) { 
		?>
		<div class="device-recap-container">
			<div class="device-recap-header">
				<?= $device->getFriendlyName(); ?>
			</div>
			<div class="device-recap-content">
				<font style="font-weight: 600; color: #333333;">seriale:</font>
				<font><?= $context["devices"][$i]->getSerial(); ?></font>
				<br>
				<br>
				<font style="font-weight: 600; color: #333333;">ultima sincronizzazione: </font>
				<font> 
					<?php 
						$lastLog = $context["lastLogs"][$i];
						if($lastLog === null){
							print("mai");
						}else{
							print($lastLog->getSyncDate()->format("d-M-Y"));
						}
					?>
				</font>
			</div>
		</div>
		<?php
			}
		?>
	</div>
	<div class="recent-activity">
		<div id="graph-container">
			
		</div>
	</div>
	
</div>
<script src="https://code.highcharts.com/highcharts.src.js"></script>

<script type="text/javascript">
		$.getJSON("/viewlog.php?metadata=1", function (data){
			const chart = new Highcharts.Chart({
			    chart: {
			        renderTo: 'graph-container',
			        defaultSeriesType: 'line',
			        animation: false,
			    },
			    title: {
			        text: 'Attività recenti'
			    },
			    xAxis: {
			        type: 'time',
			        tickPixelInterval: 20,
			        categories : data["dates"]
			    },
			    yAxis: {
			        minPadding: 0.2,
			        maxPadding: 0.2,
			        title: {
			            text: '',
			            margin: 80
			        },
			        min: 0,
			        max: 10
			    },
			    plotOptions: {
			        spline: {
			            marker: {
			                enabled: false
			            },
			            animation: false,
			            shadow: false
			        },
			        line: {
			            marker: {
			                enabled: false
			            }
			        },
			    },
			    series: [{
			        name: 'Log registrati',
			        data: data["data"].map((str) => {return parseInt(str)})
			    }]
			});

		});
	 	
</script>