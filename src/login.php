<?php

include_once __DIR__."/control/LoginController.php";

$controller = new LoginController();
try{
	$controller->run();
}catch (Exception $e){
	include_once __DIR__."/control/ErrorController.php";
	$errorController = new ErrorController($e);
	$errorController->run();
}