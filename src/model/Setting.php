<?php

class Setting implements JsonSerializable{
	
	public function __construct($id, $lba, $laa, $sync_freq, $wifi_ssid, $wifi_pwd, $wifi_sec_type, $deviceID = null){
		$this->id = $id;
		$this->lba = $lba;
		$this->laa = $laa;
		$this->sync_freq = $sync_freq;
		$this->wifi_ssid = $wifi_ssid;
		$this->wifi_pwd = $wifi_pwd;
		$this->wifi_sec_type = $wifi_sec_type;
		$this->deviceID = $deviceID;
	}

	public function getLogTimeBeforeAccident(){
		return $this->lba;
	}

	public function setLogTimeBeforeAccident($lba){
		$this->lba = $lba;
	}

	public function getLogTimeAfterAccident(){
		return $this->laa;
	}

	public function setLogTimeAfterAccident($laa){
		$this->laa = $laa;
	}

	public function getSynchronizationFrequency(){
		return $this->sync_freq;
	}

	public function setSynchronizationFrequency($freq){
		$this->sync_freq = $freq;
	}

	public function getWifiSSID(){
		return $this->wifi_ssid;
	}

	public function getWifiPassword(){
		return $this->wifi_pwd;
	}

	public function setWifiPassword($pwd){
		$this->wifi_pwd = $pwd;
	}

	public function getWifiSecurityType(){
		return $this->wifi_sec_type;
	}

	public function setWifiSecurityType($sec){
		$this->wifi_sec_type = $sec;
	}
	
	public function getDeviceID(){
		return $this->deviceID;
	}

	public function setDeviceID($deviceID){
		$this->deviceID = $deviceID;
	}

	public function setID($id){
		$this->id = $id;
	}

	public function getID(){
		return $this->id;
	}

	public function setWifiSSID($wifiSSID){
		$this->wifi_ssid = $wifiSSID;
	}

	public function jsonSerialize(){
		$result = array(
			"log_time_before_accident" => $this->lba,
			"log_time_after_accident" => $this->laa,
			"sync_freq" => $this->sync_freq,
			"wifi_ssid" => $this->wifi_ssid,
			"wifi_pwd" => $this->wifi_pwd, 
			"wifi_enc_type" => $this->wifi_sec_type,
			);
		return $result;
	}

	public static function jsonDeserialize($json){
		$dictionary = json_decode($json, true);
		if(isset($dictionary["log_time_before_accident"]) 
			&& isset($dictionary["log_time_after_accident"]) 
			&& isset($dictionary["sync_freq"])
			&& isset($dictionary["wifi_ssid"]) 
			&& isset($dictionary["wifi_pwd"]) 
			&& isset($dictionary["wifi_enc_type"]) 
			&& true){
			$setting = new Setting(
				null, $dictionary["log_time_before_accident"], 
				$dictionary["log_time_after_accident"], 
				$dictionary["sync_freq"], 
				$dictionary["wifi_ssid"], 
				$dictionary["wifi_pwd"], 
				$dictionary["wifi_enc_type"]);
		return $setting;
		}else{
			throw new Exception("Missing values in provided json");
		}
	}
}