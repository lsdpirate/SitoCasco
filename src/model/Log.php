<?php

class Log{

	public function __construct($id, $data, $sync_date, $deviceID){
		$this->id = $id;
		$this->data = $data;
		$this->sync_date = $sync_date;
		$this->deviceID = $deviceID;
	}

	public function getData(){
		return $this->data;
	}

	public function setData($data){
		$this->data = $data;
	}

	public function getSyncDate(){
		return $this->sync_date;
	}

	public function getDeviceID(){
		return $this->deviceID;
	}

	public function setSyncDate($syncdate){
		$this->synctime = $synctime;
	}

	public function setDeviceID($deviceID){
		$this->deviceID = $deviceID;
	}

	public function __toString(){
		$dateToString = $this->sync_date->format(DateTime::W3C);
		return "Log Object {data: $this->data, sync_date: $dateToString, deviceID: $this->deviceID}";
	}

	public function getID(){
		return $this->id;
	}

	public function setID($id){
		$this->id = $id;
	}

}