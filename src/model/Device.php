<?php

class Device{

	public function __construct($id, $serial, $ownerID, $friendlyName, $apiKey = null){
		$this->id = $id;
		$this->serial = $serial;
		$this->ownerID = $ownerID;
		$this->apiKey = $apiKey;
		$this->friendlyName = $friendlyName;
	}

	public function getOwnerID(){
		return $this->ownerID;
	}

	public function getSerial(){
		return $this->serial;
	}

	public function setOwnerID($id){
		$this->ownerID = $id;
	}

	public function setSerial($serial){
		$this->serial = $serial;
	}

	public function getID(){
		return $this->id;
	}

	public function setID($id){
		$this->id = $id;
	}

	public function getAPIKey(){
		if($this->apiKey === null){
			$this->apiKey = base64_encode(md5($this->serial, true));
		}
		return $this->apiKey;
	}

	public function setAPIKey($key){
		$this->apiKey = $key;
	}

	public function getFriendlyName(){
		return $this->friendlyName;
	}

	public function setFriendlyName($name){
		$this->friendlyName = $name;
	}

	public function __toString(){
		return "Device Object: {id: $this->id, serial: $this->serial, ownerID: $this->ownerID}";
	}
}