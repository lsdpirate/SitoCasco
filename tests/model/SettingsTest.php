<?php

use PHPUnit\Framework\TestCase;
include_once __DIR__."/../../src/model/Setting.php";

final class SettingsTest extends TestCase{
	
	/**
	 * @before
	 */
	public function setUp(){
		$this->settings = new Setting(null, 10, 5, 7, "myfi", "password", "WPA2", 3);
	}

	public function testJSONSerialization(){
		$json = json_encode($this->settings);
		$expected = "{\"log_time_before_accident\":10,\"log_time_after_accident\":5,\"sync_freq\":7,\"wifi_ssid\":\"myfi\",\"wifi_pwd\":\"password\",\"wifi_enc_type\":\"WPA2\"}";
		$this->assertEquals($expected, $json);
	}
}